﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data.Entity;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using RallyDemo.Dto;
using RallyDemo.Models;
using RallyDemo.Task.Mappings;
using RallyDemo.Utils;

namespace RallyDemo.Controllers
{
    [RoutePrefix("api")]
    public class EmailController : ApiControllerWithHub<EmailHub>
    {
        private RallyDemoContext _db = new RallyDemoContext();
        private EmailAndEmailDtoMapper _mapper = new EmailAndEmailDtoMapper();
        
        [HttpGet]
        [Route("emails")]
        public async Task<HttpResponseMessage> Get(string groups=null)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(groups))
                {
                    return await GetAllEmails();
                }
                return await GetByGroup(groups);

            }
            catch (Exception ex)
            {
                Trace.WriteLine("{0} ***Error Occured: \n {1}".With(GetType(),ex.ShowAllMessages()));
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.ShowAllMessages());
            }
        }

        private async Task<HttpResponseMessage> GetAllEmails()
        {
            var cts = new CancellationTokenSource();
            var emails = await _db.Emails.ToListAsync(cts.Token);
            return Request.CreateResponse(HttpStatusCode.OK, _mapper.Map(emails));
        }
        private async Task<HttpResponseMessage> GetByGroup(string groups)
        {
            string[] splitGroups = groups.Split(',');
            var emailDtos = await GetEmailsEmailDtos(splitGroups);
            return Request.CreateResponse(HttpStatusCode.OK, emailDtos);
        }

        private async Task<IList<EmailDto>> GetEmailsEmailDtos(string[] splitGroups)
        {
            var cts = new CancellationTokenSource();
            var emailByGroupsQuery = (from email in _db.Emails
                let isCorrectGroup = splitGroups.Contains(email.Group)
                where isCorrectGroup
                orderby email.LastModifiedTimestamp descending
                select email);

            var emails = await emailByGroupsQuery.ToListAsync(cts.Token);
            var emailDtos = _mapper.Map(emails);
            return emailDtos;
        }

        [HttpGet]
        [Route("emails/{id}")]
        public async Task<HttpResponseMessage> Get(int id)
        {
            try
            {
                var email = await _db.Emails.FirstOrDefaultAsync(x => x.Id == id);
                return email == null
                    ? Request.CreateErrorResponse(HttpStatusCode.NotFound, "[NotFound] Email with Id {0}".With(id))
                    : Request.CreateResponse(HttpStatusCode.OK, _mapper.Map(email));


            }
            catch (Exception ex)
            {
                Trace.WriteLine("{0} ***Error Occured: \n {1}".With(GetType(), ex.ShowAllMessages()));
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.ShowAllMessages());
            }
        }

        [HttpPost]
        [Route("emails")]
        public async Task<HttpResponseMessage> Post()
        {
            try
            {
                EmailDto emailDto = null;
                if (Request.Content.IsMimeMultipartContent("form-data")) //emails with attachments
                {
                    var provider = new MultipartFormDataStreamProvider(Path.GetTempPath());

                    await Request.Content.ReadAsMultipartAsync(provider);

                    if (provider.FormData != null)
                    {
                        emailDto = GetEmailDto(provider);
                        emailDto.AttachmentDtos = GetAttachments(provider);
                    }
                }
                else if (Request.Content.IsFormData()) //emails without attachments
                {
                    var formDataNameValuCollection = Request.Content.ReadAsFormDataAsync().Result;
                    emailDto = GetEmailDto(formDataNameValuCollection);
                }
                else
                {
                    throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
                }

                var email = _mapper.Map(emailDto);
                NotifySubscribers(emailDto, email);
                _db.Emails.Add(email);
                var cts = new CancellationTokenSource();
                await _db.SaveChangesAsync(cts.Token);
            }
            catch (Exception ex)
            {
                Trace.WriteLine("{0} *** Error Occured:\n {1}".With(GetType().ToString(), ex));
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.ShowAllMessages());
            }


            return Request.CreateResponse(HttpStatusCode.Created);
        }
        #region PrivateHelpers

        private void NotifySubscribers(EmailDto emailDto, Email email)
        {
            emailDto.ReceivedDateTime = email.LastModifiedTimestamp.ToString();
            emailDto.AttachmentDtos = null;//Purge attachemnts for push notification through signalR.
            var subscribed = Hub.Clients.Group(emailDto.Group);
            subscribed.newEmail(emailDto);
        }

        private IList<AttachmentDto> GetAttachments(MultipartFormDataStreamProvider provider)
        {
            return (from data in provider.FileData
                let fileName = data.Headers.ContentDisposition.FileName
                where !string.IsNullOrWhiteSpace(fileName)
                select new AttachmentDto
                {
                    FileName = fileName, ContentType = data.Headers.ContentType.MediaType, Content = GetStreamContent(data)
                }).ToList();
        }

        private Stream GetStreamContent(MultipartFileData data)
        {
            Stream fileStream = new MemoryStream();

            var firstfile = data.LocalFileName;
            using (var filestream = File.OpenRead(firstfile))
            {
                filestream.Seek(0, SeekOrigin.Begin);
                filestream.CopyTo(fileStream);

                return fileStream;
            }
        }

        private static EmailDto GetEmailDto(NameValueCollection formdata) 
        {

            return new EmailDto
            {
                AttachmentCount = formdata.GetFirstValue("attachment-count"),
                Bodyplain = formdata.GetFirstValue("body-plain"),
                Bodyhtml = formdata.GetFirstValue("body-html"),
                Strippedtext = formdata.GetFirstValue("stripped-text"),
                From = formdata.GetFirstValue("from"),
                To = formdata.GetFirstValue("To"),
                Recipient = formdata.GetFirstValue("recipient"),
                Subject = formdata.GetFirstValue("subject"),
                Timestamp = formdata["timestamp"],
                Group = ParseGroupFromSubject(formdata["subject"])
            };
        }

        private static string ParseGroupFromSubject(string subject)
        {
            if (string.IsNullOrWhiteSpace(subject))
                throw new ArgumentException("The email subject cannot be null or whitespace");

            int startIndex = subject.IndexOf("[");
            if (startIndex == -1)
                throw new ArgumentException("The email subject has an invalid format. Missing [ for intended group");
            int endIndex = subject.IndexOf("]");

            if (endIndex == -1)
                throw new ArgumentException("The email subject has an invalid format. Missing ] for intended group");
            startIndex = startIndex + 1;
            endIndex = endIndex - startIndex;
            var header = subject.Substring(startIndex, endIndex);
            if (string.IsNullOrWhiteSpace(header))
                throw new ArgumentException("The email subject has an invalid format. Group cannot be null or whitespace");

            return header;
        }

        private static EmailDto GetEmailDto(MultipartFormDataStreamProvider provider)
        {
            return GetEmailDto(provider.FormData);
        }

        #endregion PrivateHelpers
    }
}
