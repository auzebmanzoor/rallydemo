﻿using System;
using System.IO;
using RestSharp;
using RestSharp.Authenticators;

namespace EmailSender
{

    class Program
    {
        const string Bodyhtml =
    "<div dir=\"ltr\"><div class=\"gmail_quote\"><div class=\"HOEnZb\"><div class=\"h5\"><div dir=\"ltr\"><div class=\"gmail_quote\"><div dir=\"ltr\">Hi everyone,<div><br></div><div>I have no tasks for today.</div><div><br></div><div>Regards</div><span><font color=\"#888888\"><div><br></div><div>Auzeb</div></font></span></div>\r\n</div><br></div>\r\n</div></div></div><br></div>\r\n";
        const string BodyPlain = "Hi everyone,\r\n\r\nI have no tasks for today.\r\n\r\nRegards\r\n\r\nAuzeb\r\n";
        const string Strippedtext = "Hi everyone,\r\n\r\nAttached are my tasks for today.";
        const string Subject = "[{0}] To do list for today";
        static void Main(string[] args)
        {
            string dailyUpdates = "Daily Updates";
            string funcProgramming = "Functional Programming";
            string dbDesign = "Database Design";
            //local host
            string uri = "http://localhost:64358/api/emails"; 
            //var response1 = SendSimpleMessageWithoutAttachment(uri, "microsoft", funcProgramming);
            var response = SendSimpleMessageWithAttachment(uri, "microsoft", dbDesign);
            //var response3 =CreateRoute();
            //var response4 = GetRoutes();
            Console.WriteLine(response.StatusCode);
            //Console.ReadLine();
        }
        public static IRestResponse SendSimpleMessageWithAttachment(string uri, string username, string group)
        {
            RestClient client = new RestClient
            {
                BaseUrl = new Uri(uri),
                Authenticator = new HttpBasicAuthenticator("api",
                    "key-dd0e7936cffafd5a82c027fe3614e12f")
            };
            RestRequest request = new RestRequest();
            request.AddParameter("from", "Excited User <mailgun@sandboxfd91200ff766472c9333f1e075ad52aa.mailgun.org>");
            request.AddParameter("to",
                string.Format("{0}@sandboxfd91200ff766472c9333f1e075ad52aa.mailgun.org", username));
            request.AddParameter("subject", string.Format(Subject, group));
            request.AddParameter("body-plain", BodyPlain);
            request.AddParameter("text", BodyPlain);
            request.AddParameter("body-html", Bodyhtml);
            request.AddParameter("attachment-count", "1");
            request.AddFile("attachment", Path.Combine("Attachments", "Notes.txt"), "text/plain");
            request.Method = Method.POST;
            return client.Execute(request);
        }

        public static IRestResponse SendSimpleMessageWithoutAttachment(string uri, string username, string group)
        {
            RestClient client = new RestClient
            {
                BaseUrl = new Uri(uri),
                Authenticator = new HttpBasicAuthenticator("api",
                    "key-dd0e7936cffafd5a82c027fe3614e12f")
            };
            RestRequest request = new RestRequest();
            request.AddParameter("from", "Excited User <mailgun@sandboxfd91200ff766472c9333f1e075ad52aa.mailgun.org>");
            request.AddParameter("to",
                string.Format("{0}@sandboxfd91200ff766472c9333f1e075ad52aa.mailgun.org", username));
            request.AddParameter("subject", string.Format(Subject, group));
            request.AddParameter("body-plain", BodyPlain);
            request.AddParameter("text", BodyPlain);
            request.AddParameter("body-html", Bodyhtml);
            request.Method = Method.POST;
            return client.Execute(request);
        }

        public static IRestResponse CreateRoute()
        {
            RestClient client = new RestClient();
            client.BaseUrl = new Uri("https://api.mailgun.net/v3");
            client.Authenticator =
                    new HttpBasicAuthenticator("api",
                                               "key-dd0e7936cffafd5a82c027fe3614e12f");
            RestRequest request = new RestRequest();
            request.Resource = "routes";
            request.AddParameter("priority", 0);
            request.AddParameter("description", "Sample route");
            request.AddParameter("expression",
                                 "match_recipient('.*@sandboxfd91200ff766472c9333f1e075ad52aa.mailgun.org')");
            request.AddParameter("action",
                                 "forward('http://auzeb.com/emails/')");
            request.AddParameter("action", "stop()");
            request.Method = Method.POST;
            return client.Execute(request);
        }

        public static IRestResponse GetRoutes()
        {
            RestClient client = new RestClient();
            client.BaseUrl = new Uri("https://api.mailgun.net/v3");
            client.Authenticator =
                    new HttpBasicAuthenticator("api",
                                               "key-dd0e7936cffafd5a82c027fe3614e12f");
            RestRequest request = new RestRequest();
            request.Resource = "routes";
            request.AddParameter("skip", 1);
            request.AddParameter("limit", 1);
            return client.Execute(request);
        }

        /*

        public static IRestResponse SendSimpleMessagewithAttachment(string username)
        {
            RestClient client = new RestClient
            {
                BaseUrl = new Uri("https://api.mailgun.net/v3"),
                Authenticator = new HttpBasicAuthenticator("api",
                    "key-dd0e7936cffafd5a82c027fe3614e12f")
            };
            RestRequest request = new RestRequest();
            request.AddParameter("domain",
                "sandboxfd91200ff766472c9333f1e075ad52aa.mailgun.org", ParameterType.UrlSegment);
            request.Resource = "{domain}/messages";
            request.AddParameter("from", "Excited User <mailgun@sandboxfd91200ff766472c9333f1e075ad52aa.mailgun.org>");
            request.AddParameter("to",
                string.Format("{0}@sandboxfd91200ff766472c9333f1e075ad52aa.mailgun.org", username));
            request.AddParameter("subject", "[DailyUpdates] Tasks with attachment");
            request.AddParameter("text", "Testing some Mailgun awesomness!");
            request.AddFile("attachment", Path.Combine("Attachments", "Notes.txt"));
            request.Method = Method.POST;
            return client.Execute(request);
        }
         
        public static IRestResponse SendSimpleMessage(string username)
        {
            RestClient client = new RestClient();
            client.BaseUrl = new Uri("https://api.mailgun.net/v3");
            client.Authenticator =
                new HttpBasicAuthenticator("api",
                    "key-dd0e7936cffafd5a82c027fe3614e12f");
            RestRequest request = new RestRequest();
            request.AddParameter("domain",
                "sandboxfd91200ff766472c9333f1e075ad52aa.mailgun.org", ParameterType.UrlSegment);
            request.Resource = "{domain}/messages";
            request.AddParameter("from", "Excited User <mailgun@sandboxfd91200ff766472c9333f1e075ad52aa.mailgun.org>");
            request.AddParameter("to",
                string.Format("{0}@sandboxfd91200ff766472c9333f1e075ad52aa.mailgun.org", username));
            request.AddParameter("subject", "[DailyUpdates] Tasks");
            request.AddParameter("text", "Testing some Mailgun awesomness!");
            request.Method = Method.POST;
            return client.Execute(request);
        }

        public static IRestResponse SendSimpleMessagewithAttachment(string username)
        {
            RestClient client = new RestClient
            {
                BaseUrl = new Uri("http://localhost:64358/api/email"),
                Authenticator = new HttpBasicAuthenticator("api",
                    "key-dd0e7936cffafd5a82c027fe3614e12f")
            };
            RestRequest request = new RestRequest();
            request.AddParameter("domain",
                "sandboxfd91200ff766472c9333f1e075ad52aa.mailgun.org", ParameterType.UrlSegment);
            request.Resource = "{domain}/messages";
            request.AddParameter("from", "Excited User <mailgun@sandboxfd91200ff766472c9333f1e075ad52aa.mailgun.org>");
            request.AddParameter("to",
                string.Format("{0}@sandboxfd91200ff766472c9333f1e075ad52aa.mailgun.org", username));
            request.AddParameter("subject", "[DailyUpdates] Tasks with attachment");
            request.AddParameter("text", "Testing some Mailgun awesomness!");
            request.AddFile("Notes.txt", Path.Combine("Attachments", "Notes.txt"));
            request.Method = Method.POST;
            return client.Execute(request);
        }
         */
    }
}
