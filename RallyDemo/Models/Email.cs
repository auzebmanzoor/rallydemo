﻿using System;

namespace RallyDemo.Models
{
    public class Email : Entity<Email>
    {
        public Email()
        {
            LastModifiedTimestamp = DateTime.Now;
        }
        public string Recipient { get; set; }
        public string Sender { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public string Subject { get; set; }
        public string Group { get; set; }
        public string Strippedtext { get; set; }
        public string Strippedsignature { get; set; }
        public string Bodyhtml { get; set; }
        public string Bodyplain { get; set; }
        public string Strippedhtml { get; set; }
        public string AttachmentCount { get; set; }
        public string Timestamp { get; set; }
        public string Token { get; set; }
        public string Signature { get; set; }
        public string Messageheaders { get; set; }
        public string Contentidmap { get; set; }
    }
}
/*
recipient	string	recipient of the message as reported by MAIL TO during SMTP chat.
sender	string	sender of the message as reported by MAIL FROM during SMTP chat. Note: this value may differ from From MIME header.
from	string	sender of the message as reported by From message header, for example “Bob <bob@example.com>”.
subject	string	subject string.
body-plain	string	text version of the email. This field is always present. If the incoming message only has HTML body, Mailgun will create a text representation for you.
stripped-text	string	text version of the message without quoted parts and signature block (if found).
stripped-signature	string	the signature block stripped from the plain text message (if found).
body-html	string	HTML version of the message, if message was multipart. Note that all parts of the message will be posted, not just text/html. For instance if a message arrives with “foo” part it will be posted as “body-foo”.
stripped-html	string	HTML version of the message, without quoted parts.
attachment-count	int	how many attachments the message has.
attachment-x	string	attached file (‘x’ stands for number of the attachment). Attachments are handled as file uploads, encoded as multipart/form-data.
timestamp	int	number of second passed since January 1, 1970 (see securing webhooks).
token	string	randomly generated string with length 50 (see securing webhooks).
signature	string	string with hexadecimal digits generate by HMAC algorithm (see securing webhooks).
message-headers	string	list of all MIME headers dumped to a json string (order of headers preserved).
content-id-map	string	JSON-encoded dictionary which maps Content-ID (CID) of each attachment to the corresponding attachment-x parameter. This allows you to map posted attachments to tags like <img src='cid'> in the message body.
*/