﻿using System.Collections.Generic;
using AutoMapper;
using RallyDemo.Dto;
using RallyDemo.Models;
using RallyDemo.Utils;

namespace RallyDemo.Task.Mappings
{
    public class EmailAndEmailDtoMapper
    {
        public EmailAndEmailDtoMapper()
        {
            Mapper.CreateMap<Email, EmailDto>().ForMember(x=>x.ReceivedDateTime, src=>src.MapFrom(e=>e.LastModifiedTimestamp) );
            Mapper.CreateMap<EmailDto, Email>();
        }

        public Email Map(EmailDto source)
        {
            return Mapper.Map<Email>(source);
        }

        public EmailDto Map(Email source)
        {
            return Mapper.Map<EmailDto>(source);
        }

        public IList<EmailDto> Map(IList<Email> emails)
        {
            return emails == null ? new List<EmailDto>() : Mapper.Map<IList<EmailDto>>(emails);
        }
    }
}