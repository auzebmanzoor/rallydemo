﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(RallyDemo.Startup))]
namespace RallyDemo
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            app.MapSignalR();
        }
    }
}
