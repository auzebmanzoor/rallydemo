﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using RallyDemo.Dto;

namespace RallyDemo
{
    [HubName("email")]
    public class EmailHub : Hub
    {
        public void SendEmail(EmailDto email)
        {
            //may have to camel case the ori
           Clients.Group(email.Group).newEmail(email);
        }

        public void JoinGroups(string groups)
        {
            if (string.IsNullOrWhiteSpace(groups)) 
                throw new ArgumentException("groups cannot be null or whitespace");
            string[] splitGroups = groups.Split(',');
            foreach (var groupName in splitGroups)
            {
                Groups.Add(Context.ConnectionId, groupName);
            }
        }
    }
}