﻿using System;

namespace RallyDemo.Models
{
    public class Attachments: Entity<Attachments>
    {
        public Attachments()
        {
            LastModifiedTimestamp = DateTime.Now;
        }

        public string FileName { get; set; }
        public string Uri { get; set; }
    }
}