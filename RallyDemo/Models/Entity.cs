﻿using System;
using System.ComponentModel.DataAnnotations;

namespace RallyDemo.Models
{
    public abstract class Entity<T> : IEntity where T : class, IEntity
    {
        [Key]
        public virtual int Id { get; set; }
        public DateTime LastModifiedTimestamp { get; set; }

        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            var other = obj as T;
            if (other == null) return false;
            return Id == other.Id;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}