***************Email Routing based on email Subject***************

Note: The solution has been implemented as a proof of concept in a hackathon coding style for routing emails to a web client and storing them in a database.
There was no particular focus on code quality/best practices. Also this would not be the ideal way to achive the scnario in a real world production application. 

Tools/Technologies:

Visual Studio 2013, sql azure, Asp.net mvc 5, signalR, Angularjs.

Instructions:

Contains 2 solutions EmailSender.sln (helper utility) and EmailRouter.sln (The main solution).

Enable package restore on solutions. Run Emailrouter.

For testing locally use the EmailSender.sln console application to send an email to app on the local machine.


The app has been deployed Microsoft Azure (http://rallydemo.auzeb.com).

For testing the production application send emails to: microsoft@sandboxfd91200ff766472c9333f1e075ad52aa.mailgun.org using [] to denote the group e.g [Daily Updates]

The deployed application relies on Mailgun for routing emails to the API.

Currently a user is subscribed to all groups (new groups are only acknowledged when a page is refreshed) and attachments aren't supported.