﻿(function() {
    'use strict';
    angular.module('rallydemoapp').controller("EmailsController", EmailsController);

    EmailsController.$inject = ['$http', '$scope', '$timeout', '$log', 'hubconnection', '$sce'];

    function EmailsController($http, $scope, $timeout, $log, hubconnection, $sce, toaster) {
        var vm = this;
        vm.emails = [];
        vm.userId = 1;
        vm.groups = null;
        vm.newEmails = [];
        load();
        function load() {
            var onSuccess = function(response) {
                vm.emails = response.data;
            }
            var onUserSuccess = function(response) {
                vm.groups = response.data;
                $http.get("api/emails" + "?groups=" + vm.groups).then(onSuccess, onFailure);
                vm.joinGroups();
            }

            var onFailure = function(err) {
                $log.error(err);
            }
            
            $http.get("api/users/"+vm.userId+"/groups").then(onUserSuccess, onFailure);

        }

        vm.trustHtml = function(html) {
            return $sce.trustAsHtml(html);
        };

        vm.joinGroups = function () {
            hubconnection.hub.start().done(function () {
                console.log('Now connected, connection ID=' + $.connection.hub.id);
                hubconnection.email.server.joinGroups(vm.groups);
            })
            .fail(function () { console.log('Could not Connect!'); });
        };

        hubconnection.email.client.newEmail = function onNewEmail(email) {
            vm.addToEmails(email);
            $scope.$apply();
            console.log(email);
        };
        vm.addToEmails = function (email) {
            vm.emails.splice(0, 0, email);
            vm.newEmails.push(email);
        };
        vm.closeAlert = function (index) {
            vm.newEmails.splice(index, 1);
        };
    }
})();