﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net.Http;
using System.Text;
using Newtonsoft.Json;

namespace RallyDemo.Utils
{

    public static class StringExtensions
    {
        public static string With(this string format, params object[] args)
        {
            if (string.IsNullOrEmpty(format))
                ThrowException();
            return args != null ? string.Format(format, args) : string.Format("{0}".With(format));
        }

        public static bool IgnoreCaseEquals(this string expected, string actual)
        {
            return expected.Equals(actual, StringComparison.OrdinalIgnoreCase);
        }

        public static T ConvertTo<T>(this string json, JsonSerializerSettings settings = null)
        {
            return settings == null
                ? JsonConvert.DeserializeObject<T>(json)
                : JsonConvert.DeserializeObject<T>(json, settings);
        }

        public static Stream ToStream(this string item)
        {
            var outBytes = Encoding.ASCII.GetBytes(item);
            var stream = new MemoryStream();
            stream.Write(outBytes, 0, outBytes.Length);
            stream.Position = 0;

            return stream;
        }

        private static void ThrowException()
        {
            var stackTrace = new StackTrace();
            var frame = stackTrace.GetFrame(2);
            var method = frame.GetMethod();
            var className = method.DeclaringType == null ? string.Empty : method.DeclaringType.Name;
            throw new ApplicationException("Error formatting string. Input string is null at {0}.{1}(...)".With(className, method.Name));
        }

    }

    public static class DateTimeExtensions
    {
        public static DateTime StripTimezone(this DateTime source)
        {
            return new DateTime(source.Year, source.Month, source.Day, source.Hour, source.Minute, source.Second,
                source.Millisecond, DateTimeKind.Unspecified);
        }
    }
}