﻿using System.Diagnostics;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using RallyDemo.Dto;
using RallyDemo.Utils;

namespace RallyDemo.Http
{
    public class EmailFormDataRequest
    {
        private readonly HttpRequestMessage _request;

        public EmailFormDataRequest(HttpRequestMessage request)
        {
            _request = request;
        }

        public async Task<EmailDto> GetEmail()
        {
            EmailDto emailDto = null;
            if (_request.Content.IsMimeMultipartContent("form-data")) //emails with attachments
            {
                var provider = new MultipartFormDataStreamProvider(Path.GetTempPath());

                await _request.Content.ReadAsMultipartAsync(provider);

                if (provider.FormData != null)
                {
                    return await new TaskFactory().StartNew(() => GetEmailDto(provider));
                }
                foreach (MultipartFileData file in provider.FileData)
                {
                    Trace.WriteLine(file.Headers.ContentDisposition.FileName);
                    Trace.WriteLine(file.Headers.ContentType);

                    Trace.WriteLine("Server file path: " + file.LocalFileName);
                }
                return emailDto;
            }
            if (!_request.Content.IsFormData()) return null;

            var result = _request.Content.ReadAsFormDataAsync().Result;
            emailDto.Recipient = result["recipient"];
            emailDto.Sender = result["sender"];
            emailDto.From = result["from"];
            emailDto.To = result["To"];
            emailDto.Subject = result["subject"];
            emailDto.Group = result["subject"];//DO index of operation to get header info
            emailDto.Messageheaders = result["message-headers"];
            var strippedtext = result["stripped-text"];
            var strippedsignature = result["stripped-signature"];
            var bodyplain = result["body-plain"];
            var bodyhtml = result["body-html"];
            var strippedhtml = result["stripped-html"];
            var attachmentcount = result["attachment-count"];
            emailDto.Timestamp = result["timestamp"];
            var token = result["token"];
            var signature = result["signature"];
            return emailDto;
        }

        private static EmailDto GetEmailDto(MultipartFormDataStreamProvider provider)
        {
            return new EmailDto
            {
                AttachmentCount = provider.FormData.GetFirstValue("attachment-count"),
                Bodyplain = provider.FormData.GetFirstValue("body-plain"),
                Bodyhtml = provider.FormData.GetFirstValue("body-html"),
                Strippedtext = provider.FormData.GetFirstValue("stripped-text"),
                From = provider.FormData.GetFirstValue("from"),
                To = provider.FormData.GetFirstValue("To"),
                Recipient = provider.FormData.GetFirstValue("recipient"),
                Subject = provider.FormData.GetFirstValue("subject"),
                Timestamp = provider.FormData["timestamp"],
            };

        }

    }
}