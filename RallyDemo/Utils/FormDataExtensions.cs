﻿using System.Collections.Specialized;
using Microsoft.Ajax.Utilities;

namespace RallyDemo.Utils
{
    public static class FormDataExtensions
    {
        public static string GetFirstValue(this NameValueCollection formData, string key)
        {
            var values=formData.GetValues(key);
            return values != null ? values[0] : null;
        }

    }
}