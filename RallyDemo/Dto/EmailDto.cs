﻿using System;
using System.Collections.Generic;

namespace RallyDemo.Dto
{
    public class EmailDto
    {
        public int Id { get; set; }
        public string Recipient { get; set; }
        public string Sender { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public string Subject { get; set; }
        public string Group { get; set; }
        public string Strippedtext { get; set; }
        public string Strippedsignature { get; set; }
        public string Bodyhtml { get; set; }
        public string Bodyplain { get; set; }
        public string Strippedhtml { get; set; }
        public string AttachmentCount { get; set; }
        public string Timestamp { get; set; }
        public string Token { get; set; }
        public string Signature { get; set; }
        public string Messageheaders { get; set; }
        public string Contentidmap { get; set; }
        public string ReceivedDateTime { get; set; }
        public IList<AttachmentDto> AttachmentDtos { get; set; }
    }
}