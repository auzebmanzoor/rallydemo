﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace RallyDemo.Tests
{
    [TestFixture]
    public class Playground
    {
        [Test]
        public void Test()
        {
            var subject = "Fwd: [Functional Programming] An introduction";
            int startIndex = subject.IndexOf("[");
            if(startIndex==-1)
                throw new ArgumentException("The email subject has an invalid format. Missing [ for intended group");
            int endIndex = subject.IndexOf("]");

            if (endIndex == -1)
                throw new ArgumentException("The email subject has an invalid format. Missing ] for intended group");
            startIndex = startIndex + 1;
            endIndex = endIndex - startIndex;
            var header = subject.Substring(startIndex, endIndex);
            if(string.IsNullOrWhiteSpace(header))
                throw new ArgumentException("The email subject has an invalid format. Group cannot be null or whitespace");
            Assert.IsNotNull(header);
        }
    }
}
