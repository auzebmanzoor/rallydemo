﻿(function() {
    'use strict';
    angular.module('rallydemoapp', [
        // Angular modules 
        'ngRoute',
        'ngResource',
        //Custom modules 

        //third Party modules
    ]);
    $.connection.hub.error(function (err) {
        console.log("Error occured: " + err);
    });
    angular.module('rallydemoapp').value('hubconnection', $.connection);
    angular.module('rallydemoapp').config(function ($routeProvider, $locationProvider) {
        $routeProvider.when("/", { templateUrl: '/Angular/Emails/emails.html', controller: 'EmailsController' });
        $locationProvider.html5Mode(true);
    });
})();