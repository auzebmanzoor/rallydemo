﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using RallyDemo.Models;

namespace RallyDemo.Controllers
{
    [RoutePrefix("api")]
    public class UserController : ApiController
    {
        
        private RallyDemoContext _db = new RallyDemoContext();
        [HttpGet]
        [Route("users/{id}/groups")]
        public async Task<HttpResponseMessage> GetUserGroups(int id)//Hardcoded to return all groups
        {
            var cts = new CancellationTokenSource();
            var emailByGroupsQuery = (from email in _db.Emails

                                      select email.Group).Distinct();

            var groupsArray = await emailByGroupsQuery.ToListAsync(cts.Token);
            var groups = groupsArray.ToArray().Aggregate(string.Empty, (current, @group) => current + @group + ",").TrimEnd(',');
            return Request.CreateResponse(HttpStatusCode.OK, groups);
        }
    }
}
