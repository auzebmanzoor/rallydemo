﻿using System.IO;

namespace RallyDemo.Dto
{
    public class AttachmentDto
    {
        public string FileName { get; set; }
        public Stream Content { get; set; }
        public string ContentType { get; set; }
    }
}